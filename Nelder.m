clear;
clc;
syms x;
syms y;

%% Input Fields
% Definition for f(x)
f = x^2 - 4*x + y^2 - y - x*y;

% Initial Points
v1 = [0,0];
v2 = [1.2,0];
v3 = [0,0.8];
V = [v1;v2;v3];


%% Actual Algorithm
hold on;
% full convergence, but the graph will be ugly
% for i = 1:33
% for demo only run 10 cycles
for i = 1:10
    initial_result = [eval(subs(f,{x,y},{V(1,1),V(1,2)})),eval(subs(f,{x,y},{V(2,1),V(2,2)})),eval(subs(f,{x,y},{V(3,1),V(3,2)}))];
    % sort
    [sorted_result, sorted_index] = sort(initial_result);

    % assign by sorted result
    B = V(sorted_index(1),:);
    G = V(sorted_index(2),:);
    W = V(sorted_index(3),:);
    fB = sorted_result(1);
    fG = sorted_result(2);
    fW = sorted_result(3);
    
    % print out
    i
    B
    fB
    
    % plot triangles
    plot(V(1:3,1)',V(1:3,2)','.-');
    MassCentral = (B + G + W) / 3;
    txt = strcat('T', num2str(i), ' f=', num2str(fB));
    text(MassCentral(1),MassCentral(2),txt);

    % reflection
    M = (B + G) / 2;
    R = 2*M - W;
    fR = eval(subs(f,{x,y},{R(1),R(2)}));

    % case determination:
    if fR < fG
        % case i
        if fB < fR
            % replace W with R
            [W,R,fW,fR] = replaceVertexAndVal(W,R,fW,fR);
        else
            % compute E and f(E)
            E = 2*R - M;
            fE = eval(subs(f,{x,y},{E(1),E(2)}));
            if fE < fB
                % replace W with E
                [W,E,fW,fE] = replaceVertexAndVal(W,E,fW,fE);
            else
                % replace W with R
                [W,R,fW,fR] = replaceVertexAndVal(W,R,fW,fR);
            end
        end
    else
        % case ii
        if fR < fW
            % replace W with R
            [W,R,fW,fR] = replaceVertexAndVal(W,R,fW,fR);
        end
        
        % compute C = (W + M) / 2
        % or C = (M + R) / 2
        % and fC
        C1 = (W + M) / 2;
        C2 = (M + R) / 2;
        fC1 = eval(subs(f,{x,y},{C1(1),C1(2)}));
        fC2 = eval(subs(f,{x,y},{C2(1),C2(2)}));
        if fC1 < fC2
            C = C1;
            fC = fC1;
        else
            C = C2;
            fC = fC2;
        end
        
        if fC < fW
            % replace W with C
            [W,C,fW,fC] = replaceVertexAndVal(W,C,fW,fC);
        else
            % compute S and fS
            S = (B + W) / 2;
            fS = eval(subs(f,{x,y},{S(1),S(2)}));
            % replace W with S
            [W,S,fW,fS] = replaceVertexAndVal(W,S,fW,fS);
            % replace G with M
            fM = eval(subs(f,{x,y},{M(1),M(2)}));
            [G,M,fG,fM] = replaceVertexAndVal(G,M,fG,fM);
        end
    end
    V = [B;G;W];
end

% plot contour
u = linspace(0,4);
v = linspace(0,4);
[U,V] = meshgrid(u,v);
Z = eval(subs(f,{x,y},{U,V}));
contour(U,V,Z,15);

hold off;